import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/primeng';

import { NeatWhiteboardComponent } from './neat-whiteboard.component';

@NgModule({
  imports: [
    ButtonModule,
    CommonModule
  ],
  declarations: [ NeatWhiteboardComponent ],
  exports: [ NeatWhiteboardComponent ]
})
export class NeatWhiteboardModule { }
